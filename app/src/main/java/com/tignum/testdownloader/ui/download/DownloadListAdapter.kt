package com.tignum.testdownloader.ui.download

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tignum.testdownloader.R
import com.tignum.testdownloader.model.DownloadFileItem
import com.tignum.testdownloader.model.DownloadStatus
import com.tignum.testdownloader.utilities.toGone
import com.tignum.testdownloader.utilities.toInvisible
import com.tignum.testdownloader.utilities.toVisible

class DownloadListAdapter(
    private val context: Context,
    private val downloadList: ArrayList<DownloadFileItem>,
    private val onDownloadItemClick: OnDownloadItemClick
) : RecyclerView.Adapter<DownloadListAdapter.DownloadItemViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DownloadItemViewHolder {
        val view = inflater.inflate(R.layout.item_download, parent, false)
        return DownloadItemViewHolder(view)
    }

    override fun getItemCount(): Int = downloadList.size

    override fun onBindViewHolder(holder: DownloadItemViewHolder, position: Int) {
        holder.bind(downloadList[position])
    }

    inner class DownloadItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val fileName = view.findViewById<TextView>(R.id.item_download_txtName)
        private val fileInfo = view.findViewById<TextView>(R.id.item_download_txtInfo)
        private val pbProgress = view.findViewById<ProgressBar>(R.id.item_download_pbProgress)
        private val btnPauseOrResume =
            view.findViewById<ImageView>(R.id.item_download_btnPauseOrResume)
        private val btnPlay = view.findViewById<ImageView>(R.id.item_download_btnPlay)
        private val btnDelete = view.findViewById<ImageView>(R.id.item_download_btnDelete)

        fun bind(downloadFileItem: DownloadFileItem) {
            fileName.text = downloadFileItem.name
            fileInfo.text = downloadFileItem.createdAt
            when (downloadFileItem.status) {
                DownloadStatus.INIT -> {
                    init()
                }
                DownloadStatus.PROGRESS -> {
                    pbProgress.progress = downloadFileItem.progress
                    progress()
                }
                DownloadStatus.PAUSE -> {
                    pbProgress.progress = downloadFileItem.progress
                    pause()
                }
                DownloadStatus.ERROR -> {
                    error()
                }
                DownloadStatus.COMPLETE -> {
                    complete()
                }
            }

            btnDelete.setOnClickListener {
                onDownloadItemClick.onDeleteClick(downloadFileItem)
            }

            btnPauseOrResume.setOnClickListener {
                onDownloadItemClick.onPauseOrResumeClick(downloadFileItem)
            }

            btnPlay.setOnClickListener {
                onDownloadItemClick.onPlayClick(downloadFileItem)
            }
        }

        private fun init() {
            pbProgress.toVisible()
            pbProgress.isIndeterminate = true
            btnPauseOrResume.toGone()
            btnPlay.toGone()
            btnDelete.toGone()
        }

        private fun progress() {
            btnPauseOrResume.toVisible()
            btnPauseOrResume.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_pause
                )
            )
            pbProgress.toVisible()
            pbProgress.isIndeterminate = false
            btnPlay.toGone()
            btnDelete.toGone()
        }

        private fun pause() {
            btnPauseOrResume.toVisible()
            btnPauseOrResume.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_download
                )
            )
            pbProgress.toVisible()
            pbProgress.isIndeterminate = false
            btnPlay.toGone()
            btnDelete.toGone()
        }

        private fun error() {
            btnPauseOrResume.setImageDrawable(
                ContextCompat.getDrawable(context, R.drawable.ic_download)
            )
            pbProgress.toGone()
            btnPlay.toGone()
            btnDelete.toVisible()
        }

        private fun complete() {
            btnPauseOrResume.toInvisible()
            pbProgress.toGone()
            btnPlay.toVisible()
            btnDelete.toVisible()
        }

    }

}
