package com.tignum.testdownloader.ui.player

import android.app.Activity
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import com.tignum.testdownloader.R
import kotlinx.android.synthetic.main.activity_player.*
import java.io.File

class PlayerActivity : AppCompatActivity() {

    companion object{
        private const val EXT_VIDEO_NAME = "ext_video_name"
        fun start(activity: Activity,fileName:String){
            val intent = Intent(activity,PlayerActivity::class.java)
            intent.putExtra(EXT_VIDEO_NAME,fileName)
            activity.startActivity(intent)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)

        val path = this.filesDir.absolutePath
        val videoName = intent.getStringExtra(EXT_VIDEO_NAME) ?: ""
        val videoFile = File(path,videoName)
        playVideo(videoFile)

    }

    private fun playVideo(videoFile:File){
        val mediaController = MediaController(this)
        mediaController.setAnchorView(activity_player_videoView)
        activity_player_videoView.setMediaController(mediaController)
        activity_player_videoView.setVideoURI(Uri.parse(videoFile.absolutePath))
        activity_player_videoView.start()
    }
}