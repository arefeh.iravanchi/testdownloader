package com.tignum.testdownloader.ui.download

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.tignum.testdownloader.db.DownloadEntity
import com.tignum.testdownloader.model.DownloadFileItem
import com.tignum.testdownloader.model.DownloadStatus
import com.tignum.testdownloader.repository.DownloadRepository
import com.tignum.testdownloader.utilities.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class DownloadListViewModel @Inject constructor(
    private val downloadRepository: DownloadRepository
) :
    ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private var downloadListLiveData = MutableLiveData<List<DownloadFileItem>>()

    fun downloadList(): LiveData<List<DownloadFileItem>> = downloadListLiveData
    private var downloadItemList = arrayListOf<DownloadFileItem>()

    fun fetchDownloads() {
        downloadRepository.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                downloadItemList.clear()
                it.map { file ->
                    downloadItemList.add(
                        DownloadFileItem(
                            file.name,
                            file.fileStatus,
                            file.downloadId,
                            file.url,
                            Utils.convertMillsToDate(file.cratedAt),
                            file.progress
                        )
                    )
                }
                downloadListLiveData.value = downloadItemList
            }, {}).let {
                compositeDisposable.add(it)
            }

    }

    fun checkDuplicateDownload(url: String): DownloadFileItem? {
        return downloadListLiveData.value?.find { it.url == url }
    }

    fun downloadFile(
        url: String,
        dirPath: String,
        previousDownloadItem: DownloadFileItem? = null
    ) {
        val fileName = Utils.getFileName(url)
        previousDownloadItem?.let {
            remove(it, dirPath, true)
        }
        addFile(DownloadEntity(url, fileName))
        val downloadRequest = PRDownloader.download(url, dirPath, fileName).build()
        val downloadId = downloadRequest.start(object : OnDownloadListener {
            override fun onDownloadComplete() {
                changeStatus(url, DownloadStatus.COMPLETE)
            }

            override fun onError(error: Error?) {
                changeStatus(url, DownloadStatus.ERROR)
            }
        })
        downloadRequest.setOnProgressListener { progress ->
            val updatedProgress =
                ((progress.currentBytes / progress.totalBytes.toFloat()) * 100).toInt()
            downloadItemList.find { it.downloadId == downloadId && it.progress != updatedProgress }
                ?.let {
                    updateProgress(updatedProgress, downloadId)
                }
        }.setOnStartOrResumeListener {
            updateId(url, downloadId)
            changeStatus(url, DownloadStatus.PROGRESS)
        }.setOnPauseListener {
            changeStatus(url, DownloadStatus.PAUSE)
        }


    }

    fun changeStatus(url: String, downloadStatus: DownloadStatus) {
        downloadRepository.updateStatus(url, downloadStatus)
            .subscribeOn(Schedulers.io())
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }
    }

    private fun addFile(downloadEntity: DownloadEntity) {
        downloadRepository.addItem(downloadEntity)
            .subscribeOn(Schedulers.io())
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }
    }

    private fun updateId(url: String, downloadId: Int) {
        downloadRepository.updateId(url, downloadId)
            .subscribeOn(Schedulers.io())
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }
    }

    private fun updateProgress(progress: Int, downloadId: Int) {
        downloadRepository.updateProgress(progress, downloadId)
            .subscribeOn(Schedulers.io())
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }
    }

    fun remove(
        downloadFileItem: DownloadFileItem,
        dirPath: String,
        allowToReplace: Boolean = false
    ): Boolean {
        if (downloadFileItem.status != DownloadStatus.PROGRESS
            && downloadFileItem.status != DownloadStatus.PROGRESS || allowToReplace
        ) {
            PRDownloader.cancel(downloadFileItem.downloadId)
            downloadRepository.remove(downloadFileItem.downloadId)
                .subscribeOn(Schedulers.io())
                .subscribe()
                .let {
                    compositeDisposable.add(it)
                }

            val localFile = File(dirPath, downloadFileItem.name)
            if (localFile.exists()) {
                localFile.delete()
            }
            return true
        }
        return false
    }

    fun pauseOrResume(downloadFileItem: DownloadFileItem) {
        when (downloadFileItem.status) {
            DownloadStatus.PROGRESS -> {
                PRDownloader.pause(downloadFileItem.downloadId)

            }
            DownloadStatus.PAUSE -> {
                PRDownloader.resume(downloadFileItem.downloadId)
            }
            else -> {
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}