package com.tignum.testdownloader.ui.download

import android.os.Bundle
import android.webkit.URLUtil
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.tignum.testdownloader.R
import com.tignum.testdownloader.di.Injectable
import com.tignum.testdownloader.model.DownloadFileItem
import com.tignum.testdownloader.ui.player.PlayerActivity
import kotlinx.android.synthetic.main.activity_download.*
import javax.inject.Inject

class DownloadListActivity : AppCompatActivity(), Injectable,
    OnDownloadItemClick {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private var downloadListViewModel: DownloadListViewModel? = null

    private val downloadList = arrayListOf<DownloadFileItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_download)

        downloadListViewModel =
            ViewModelProvider(this, viewModelFactory)[DownloadListViewModel::class.java]
        downloadListViewModel?.fetchDownloads()
        observeDownloadList()
        initViews()
    }

    private fun initViews() {
        activity_download_btnDownload.setOnClickListener {
            val url = activity_download_etUrl.text.toString()
            when {
                url.isBlank() -> {
                    Toast.makeText(this, getString(R.string.empty_url), Toast.LENGTH_SHORT).show()
                }
                !URLUtil.isValidUrl(url) -> {
                    Toast.makeText(this, getString(R.string.invalid_url), Toast.LENGTH_SHORT).show()
                }
                downloadListViewModel?.checkDuplicateDownload(url) != null -> {
                    showDuplicateUrlDialog(downloadListViewModel?.checkDuplicateDownload(url)!!)
                }
                else -> {
                    downloadUrlFile()
                }
            }
        }

        activity_download_rcList.apply {
            adapter = DownloadListAdapter(
                this@DownloadListActivity,
                downloadList,
                this@DownloadListActivity
            )
            layoutManager =
                LinearLayoutManager(this@DownloadListActivity, RecyclerView.VERTICAL, false)
        }
    }

    private fun observeDownloadList() {
        downloadListViewModel?.downloadList()
            ?.observe(this,
                Observer<List<DownloadFileItem>> { list ->
                    list?.let {
                        downloadList.clear()
                        downloadList.addAll(it)
                        activity_download_rcList.adapter?.notifyDataSetChanged()
                    }
                })
    }

    override fun onPauseOrResumeClick(downloadFileItem: DownloadFileItem) {
        downloadListViewModel?.pauseOrResume(downloadFileItem)
    }

    override fun onDeleteClick(downloadFileItem: DownloadFileItem) {
        downloadListViewModel?.remove(
            downloadFileItem, filesDir.absolutePath
        )
    }

    override fun onPlayClick(downloadFileItem: DownloadFileItem) {
        PlayerActivity.start(this, downloadFileItem.name)
    }

    private fun showDuplicateUrlDialog(downloadedItem: DownloadFileItem) {
        MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.duplicate_url))
            .setMessage(getString(R.string.duplicate_url_text))
            .setNegativeButton(getString(R.string.download)) { _, _ ->
                downloadUrlFile(downloadedItem)
            }
            .setPositiveButton(getString(R.string.cancel)) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    private fun downloadUrlFile(downloadedItem: DownloadFileItem? = null) {
        downloadListViewModel?.downloadFile(
            activity_download_etUrl.text.toString(),
            filesDir.absolutePath,
            downloadedItem
        )
    }

}