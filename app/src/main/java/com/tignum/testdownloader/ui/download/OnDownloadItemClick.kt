package com.tignum.testdownloader.ui.download

import com.tignum.testdownloader.model.DownloadFileItem

interface OnDownloadItemClick {

    fun onPauseOrResumeClick(downloadFileItem: DownloadFileItem)
    fun onDeleteClick(downloadFileItem: DownloadFileItem)
    fun onPlayClick(downloadFileItem: DownloadFileItem)

}