package com.tignum.testdownloader.di

import android.content.Context
import androidx.room.Room
import com.tignum.testdownloader.App
import com.tignum.testdownloader.db.AppDatabase
import com.tignum.testdownloader.db.DownloadDao
import com.tignum.testdownloader.utilities.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: App): Context = app

    @Provides
    @Singleton
    fun provideDb(app: App): AppDatabase {
        return Room
            .databaseBuilder(app, AppDatabase::class.java, DATABASE_NAME)
            .build()
    }


    @Provides
    @Singleton
    fun provideDownloadDao(db: AppDatabase): DownloadDao {
        return db.videoFileDao()
    }


}