package com.tignum.testdownloader.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tignum.testdownloader.ui.download.DownloadListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(DownloadListViewModel::class)
    abstract fun bindDownloadListViewModel(downloadListViewModel: DownloadListViewModel): ViewModel



}