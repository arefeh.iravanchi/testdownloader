package com.tignum.testdownloader.di

import com.tignum.testdownloader.ui.download.DownloadListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeDownloadListActivity(): DownloadListActivity
}