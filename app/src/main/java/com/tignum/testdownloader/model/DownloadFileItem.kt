package com.tignum.testdownloader.model

data class DownloadFileItem(
    val name: String,
    val status: DownloadStatus,
    val downloadId: Int,
    val url: String,
    val createdAt: String,
    var progress: Int
)