package com.tignum.testdownloader.model

enum class DownloadStatus {
    INIT,PROGRESS,COMPLETE,PAUSE, ERROR
}