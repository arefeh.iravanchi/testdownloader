package com.tignum.testdownloader.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tignum.testdownloader.model.DownloadStatus
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface DownloadDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun set(downloadEntity: DownloadEntity): Completable

    @Query("SELECT * FROM DownloadEntity")
    fun getAll(): Observable<List<DownloadEntity>>

    @Query("UPDATE DownloadEntity SET status = :status WHERE download_url = :url")
    fun updateStatusByUrl(url: String, status: DownloadStatus): Completable

    @Query("UPDATE DownloadEntity SET download_id = :downloadId WHERE download_url=:url")
    fun updateId(url: String, downloadId: Int): Completable

    @Query("UPDATE DownloadEntity SET progress =:progress WHERE download_id = :downloadId")
    fun updateProgress(progress:Int,downloadId: Int) : Completable

    @Query("DELETE FROM DownloadEntity WHERE download_id = :downloadId")
    fun remove(downloadId: Int) : Completable

}