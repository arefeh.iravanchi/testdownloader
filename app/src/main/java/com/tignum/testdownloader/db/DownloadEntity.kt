package com.tignum.testdownloader.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.tignum.testdownloader.model.DownloadStatus

@Entity
data class DownloadEntity(
    @PrimaryKey
    @ColumnInfo(name = "download_url") val url: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "download_id") val downloadId: Int = 0,
    @ColumnInfo(name = "status") var fileStatus: DownloadStatus = DownloadStatus.INIT,
    @ColumnInfo(name = "progress") var progress: Int = 0,
    @ColumnInfo(name = "created_at") var cratedAt: Long = System.currentTimeMillis()
)