package com.tignum.testdownloader.db

import androidx.room.TypeConverter
import com.tignum.testdownloader.model.DownloadStatus

class Converters {

    @TypeConverter
    fun downloadStatusToString(downloadStatus: DownloadStatus) : Int{
        return downloadStatus.ordinal
    }

    @TypeConverter
    fun intToDownloadStatus(value: Int) : DownloadStatus {
        return DownloadStatus.values()[value]
    }


}