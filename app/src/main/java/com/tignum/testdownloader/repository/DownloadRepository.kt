package com.tignum.testdownloader.repository

import com.tignum.testdownloader.db.DownloadDao
import com.tignum.testdownloader.db.DownloadEntity
import com.tignum.testdownloader.model.DownloadStatus
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DownloadRepository @Inject constructor(private val downloadFileDoa: DownloadDao) {


    fun getAll(): Observable<List<DownloadEntity>> {
        return downloadFileDoa.getAll()
    }

    fun addItem(item: DownloadEntity): Completable {
        return downloadFileDoa.set(item)
    }

    fun updateStatus(url: String, status: DownloadStatus): Completable {
        return downloadFileDoa.updateStatusByUrl(url, status)
    }

    fun updateId(url: String, downloadId: Int): Completable {
        return downloadFileDoa.updateId(url, downloadId)
    }

    fun updateProgress(progress: Int, downloadId: Int): Completable {
        return downloadFileDoa.updateProgress(progress, downloadId)
    }

    fun remove(downloadId: Int): Completable {
        return downloadFileDoa.remove(downloadId)
    }


}


