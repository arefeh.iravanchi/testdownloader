package com.tignum.testdownloader.utilities

import android.webkit.URLUtil
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    companion object {

        fun getFileName(url: String): String {
            return URLUtil.guessFileName(url, null, null) ?: "DownloadFile"
        }

        fun convertMillsToDate(timeInMills: Long): String {
            val cal = Calendar.getInstance()
            cal.time = Date(timeInMills)
            val month = SimpleDateFormat("MMM", Locale.getDefault()).format(cal.time)
            val year = cal[Calendar.YEAR]
            val day = cal[Calendar.DAY_OF_MONTH]
            return "$month $day,$year"
        }

    }
}