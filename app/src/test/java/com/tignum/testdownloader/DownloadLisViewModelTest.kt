package com.tignum.testdownloader

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.tignum.testdownloader.db.DownloadDao
import com.tignum.testdownloader.db.DownloadEntity
import com.tignum.testdownloader.model.DownloadFileItem
import com.tignum.testdownloader.model.DownloadStatus
import com.tignum.testdownloader.repository.DownloadRepository
import com.tignum.testdownloader.ui.download.DownloadListViewModel
import com.tignum.testdownloader.utilities.Utils
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.*
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class DownloadLisViewModelTest {

    @Mock
    lateinit var downloadDao: DownloadDao

    @InjectMocks
    lateinit var downloadRepository: DownloadRepository

    lateinit var downloadListViewModel: DownloadListViewModel

    @Mock
    lateinit var observer: Observer<List<DownloadFileItem>>

    private val sampleUrl = "https://d2gjspw5enfim.cloudfront.net/qot_web/tignum_x_video.mp4"
    private val sampleName = "sampleName"
    private val sampleDownloadId = 111
    private val dirPath = "/data/user/0/com.tignum.testdownloader/files"

    @Rule
    @JvmField
    var rule = InstantTaskExecutorRule()

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        downloadListViewModel = DownloadListViewModel(downloadRepository)
    }

    @Test
    fun fetchDownloads() {
        val sampleDownloadEntity = DownloadEntity(
            sampleUrl, sampleName
        )
        val sampleDownloadFile = DownloadFileItem(
            sampleDownloadEntity.name,
            sampleDownloadEntity.fileStatus,
            sampleDownloadEntity.downloadId,
            sampleDownloadEntity.url,
            Utils.convertMillsToDate(System.currentTimeMillis()), 0
        )

        val entities = listOf(sampleDownloadEntity)
        val expectedItems = listOf(sampleDownloadFile)
        Mockito.`when`(downloadDao.getAll())
            .thenReturn(Observable.just(entities))

        downloadListViewModel.downloadList().observeForever(observer)
        downloadListViewModel.fetchDownloads()
        Mockito.verify(observer).onChanged(expectedItems)
    }


    @Test
    fun checkDuplicateDownload() {
        val sampleDownloadEntity = DownloadEntity(
            sampleUrl, sampleName, sampleDownloadId
        )

        val expectedDuplicateFile = DownloadFileItem(
            sampleName,
            DownloadStatus.INIT,
            sampleDownloadId,
            sampleUrl,
            Utils.convertMillsToDate(System.currentTimeMillis()),
            0
        )
        val entities = listOf(sampleDownloadEntity)
        Mockito.`when`(downloadDao.getAll())
            .thenReturn(Observable.just(entities))

        downloadListViewModel.downloadList().observeForever(observer)
        downloadListViewModel.fetchDownloads()
        Assert.assertEquals(
            expectedDuplicateFile,
            downloadListViewModel.checkDuplicateDownload(sampleUrl)
        )

    }

    @Test
    fun remove() {
        val sampleProgressedFile = DownloadFileItem(
            sampleName,
            DownloadStatus.PROGRESS, sampleDownloadId, sampleUrl,
            Utils.convertMillsToDate(System.currentTimeMillis()),
            20
        )

        val sampleCompleteFile = DownloadFileItem(
            sampleName,
            DownloadStatus.COMPLETE, sampleDownloadId, sampleUrl,
            Utils.convertMillsToDate(System.currentTimeMillis()),
            100
        )

        Mockito.`when`(downloadDao.remove(sampleDownloadId))
            .thenReturn(Completable.complete())

        Assert.assertEquals(false, downloadListViewModel.remove(sampleProgressedFile, dirPath))
        Assert.assertEquals(true, downloadListViewModel.remove(sampleCompleteFile, dirPath))

    }

}