package com.tignum.testdownloader

import com.tignum.testdownloader.utilities.Utils
import org.junit.Assert
import org.junit.Test

class UtilsTest {


    private val sampleMills = 1598210072918
    private val expectedDateFormat = "Aug 23,2020"

    @Test
    fun convertMillsToDate() {
        val formattedDate = Utils.convertMillsToDate(sampleMills)
        Assert.assertNotNull(formattedDate)
        Assert.assertEquals(expectedDateFormat,formattedDate)
    }

}