# TestDownloader

## Functionality
***TestDownloader*** is a simple application, which allow users to enter file url to download and play it.
It also shows list of previous downloads and their status.

## Technical Overview
The app is developed upon MVVM architecture.
It uses RxJava, Room as a local database and Dagger2 for dependency Injection.
The expected ViewModel and Util behavior tested by JUnit Test in the test package.
